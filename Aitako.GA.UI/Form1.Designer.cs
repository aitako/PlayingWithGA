﻿namespace Aitako.GA.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.numPoints = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.cmbStrategy = new System.Windows.Forms.ComboBox();
            this.chkElitism = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numTopSelection = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.numMutationRate = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numPoolSize = new System.Windows.Forms.NumericUpDown();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTopSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMutationRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPoolSize)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(828, 832);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 228);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Stats";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(179, 191);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.numPoints);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.cmbStrategy);
            this.panel2.Controls.Add(this.chkElitism);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.numTopSelection);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.numMutationRate);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.numPoolSize);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(828, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 832);
            this.panel2.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Points";
            // 
            // numPoints
            // 
            this.numPoints.Location = new System.Drawing.Point(134, 12);
            this.numPoints.Name = "numPoints";
            this.numPoints.Size = new System.Drawing.Size(120, 20);
            this.numPoints.TabIndex = 15;
            this.numPoints.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(136, 38);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Create Points";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmbStrategy
            // 
            this.cmbStrategy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStrategy.FormattingEnabled = true;
            this.cmbStrategy.Location = new System.Drawing.Point(134, 147);
            this.cmbStrategy.Name = "cmbStrategy";
            this.cmbStrategy.Size = new System.Drawing.Size(121, 21);
            this.cmbStrategy.TabIndex = 13;
            // 
            // chkElitism
            // 
            this.chkElitism.AutoSize = true;
            this.chkElitism.Location = new System.Drawing.Point(134, 171);
            this.chkElitism.Name = "chkElitism";
            this.chkElitism.Size = new System.Drawing.Size(55, 17);
            this.chkElitism.TabIndex = 12;
            this.chkElitism.Text = "Elitism";
            this.chkElitism.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Breeding Strategy";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Top Selection (%)";
            // 
            // numTopSelection
            // 
            this.numTopSelection.Location = new System.Drawing.Point(134, 119);
            this.numTopSelection.Name = "numTopSelection";
            this.numTopSelection.Size = new System.Drawing.Size(120, 20);
            this.numTopSelection.TabIndex = 8;
            this.numTopSelection.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mutation Rate (%)";
            // 
            // numMutationRate
            // 
            this.numMutationRate.Location = new System.Drawing.Point(134, 93);
            this.numMutationRate.Name = "numMutationRate";
            this.numMutationRate.Size = new System.Drawing.Size(120, 20);
            this.numMutationRate.TabIndex = 6;
            this.numMutationRate.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pool Size";
            // 
            // numPoolSize
            // 
            this.numPoolSize.Location = new System.Drawing.Point(134, 67);
            this.numPoolSize.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numPoolSize.Name = "numPoolSize";
            this.numPoolSize.Size = new System.Drawing.Size(120, 20);
            this.numPoolSize.TabIndex = 4;
            this.numPoolSize.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 832);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTopSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMutationRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPoolSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numTopSelection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numMutationRate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numPoolSize;
        private System.Windows.Forms.CheckBox chkElitism;
        private System.Windows.Forms.ComboBox cmbStrategy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numPoints;
        private System.Windows.Forms.Button button2;
    }
}

