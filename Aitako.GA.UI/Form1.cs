﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NUnit.Framework;

namespace Aitako.GA.UI
{
    public partial class Form1 : Form
    {
        private List<Point> _pointsToVisit;

        private Algorithm<Route> GA;

        private IterationResult<Route> _result;
        private Point _startingPoint;

        private int gridSize = 800;

        public Form1()
        {
            InitializeComponent();
            SetDoubleBuffered(panel1);
            cmbStrategy.DataSource = Enum.GetNames(typeof(BreedingStrategy));
        }

        private void SetupPoints()
        {
            var rnd = new Random();
            _pointsToVisit = new List<Point>();
            _startingPoint = new Point(rnd.Next(0, gridSize), rnd.Next(0, gridSize));
            _pointsToVisit.Insert(0, _startingPoint);
            for (int i = 1; i < numPoints.Value; i++)
            {
                _pointsToVisit.Add(new Point(rnd.Next(0, gridSize), rnd.Next(0, gridSize)));
            }
        }

        private void SetupGA()
        {
            if (_pointsToVisit == null)
            {
                SetupPoints();
            }

            if (GA != null)
            {
                GA.Stop();
                Thread.Sleep(2000);
            }

            GA = new Algorithm<Route>();
            GA.UsingBreedingStrategy(
                (BreedingStrategy)Enum.Parse(typeof(BreedingStrategy), cmbStrategy.SelectedItem.ToString()))
              .WithPopulationSize((int)numPoolSize.Value)
              .UsingPercentageOfFittestGenomesToBreed((int)numTopSelection.Value)
              .WithMutationRate((int)numMutationRate.Value)
              .UsingMutationFunction((chanceOfMutation, child) =>
                  {
                      var mutation = GA.GetRandom(0, 100);
                      if (mutation <= chanceOfMutation)
                      {
                          var p1 = GA.GetRandom(0, _pointsToVisit.Count);
                          var p2 = GA.GetRandom(0, _pointsToVisit.Count);

                          var tmp = child.Points[p1];
                          child.Points[p1] = child.Points[p2];
                          child.Points[p2] = tmp;
                      }
                      return child;
                  })
              .WithComparitor((route, route1) =>
                  {
                      var points1 = new List<Point>(route.Points);
                      var points2 = new List<Point>(route1.Points);
                      return points1 == points2;
                  })
              .UsingGenomeGenerationFunction(() =>
                  {
                      var randomPoints =
                          _pointsToVisit.OrderBy(i => GA.GetRandom(int.MinValue, int.MaxValue)).ToList();
                      return new Route(randomPoints.ToArray());
                  })
              .UsingCrossOverFunction((route1, route2) =>
                  {
                      var points1 = new List<Point>(route1.Points);
                      var points2 = new List<Point>(route2.Points);

                      if (points1 == points2)
                      {
                          return new Route[0];
                      }

                      var crossOverIndex1 = GA.GetRandom(0, _pointsToVisit.Count);

                      var x = points1.IndexOf(points2[crossOverIndex1]);

                      var temp = points1[crossOverIndex1];
                      points1[crossOverIndex1] = points1[x];
                      points1[x] = temp;

                      temp = points2[crossOverIndex1];
                      points2[crossOverIndex1] = points2[x];
                      points2[x] = temp;

                      var child1 = new Route(points1.ToArray());
                      var child2 = new Route(points2.ToArray());

                      return new Route[] { child1, child2 };
                  })
              .WithFitnessFunction(route =>
                  {
                      double totalDistance = 0;
                      for (int currentPointIndex = 0;
                           currentPointIndex < route.Points.Count()-1;
                           currentPointIndex++)
                      {
                          totalDistance += Math.Sqrt(
                              (Math.Pow(route.Points[currentPointIndex].X - route.Points[currentPointIndex + 1].X, 2) +
                               Math.Pow(route.Points[currentPointIndex].Y - route.Points[currentPointIndex + 1].Y, 2)));
                      }
                      return int.MaxValue - totalDistance;
                  });

            if (chkElitism.Checked)
            {
                GA.WithElitism();
            }

            GA.Initialise();

            GA.OnIterationCompleted += GaOnOnIterationCompleted;
        }

        private void GaOnOnIterationCompleted(IterationResult<Route> result)
        {
            _result = result;
            panel1.Invoke(new MethodInvoker(panel1.Invalidate));
            this.Invoke(new MethodInvoker(this.UpdateLabel));
        }

        void UpdateLabel()
        {
            if (_result != null)
                label1.Text = string.Format("Generation {0}, Distance {1}", GA.GetGenerationNumber(), Math.Abs(GA.GetFitness() - int.MaxValue).ToString("###,##"));
        }

        public static void SetDoubleBuffered(System.Windows.Forms.Control c)
        {
            //Taxes: Remote Desktop Connection and painting
            //http://blogs.msdn.com/oldnewthing/archive/2006/01/03/508694.aspx
            if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                return;

            System.Reflection.PropertyInfo aProp =
                  typeof(System.Windows.Forms.Control).GetProperty(
                        "DoubleBuffered",
                        System.Reflection.BindingFlags.NonPublic |
                        System.Reflection.BindingFlags.Instance);

            aProp.SetValue(c, true, null);
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (_pointsToVisit != null)
            {
                e.Graphics.Clear(Color.White);
                DrawPointsToVisit(e.Graphics);
                DrawPath(e.Graphics);
            }
        }

        private void DrawPath(Graphics graphics)
        {
            if (_result != null)
            {
                for (var i = 0; i < _result.Genome.Points.Count(); i++)
                {
                    var point = _result.Genome.Points[i];
                    graphics.DrawString((i + 1).ToString(), new Font(FontFamily.GenericSansSerif, 8), Brushes.Black, point.X + 5, point.Y + 5);
                }
                graphics.DrawPolygon(Pens.Red, _result.Genome.Points);
            }
        }

        private void DrawPointsToVisit(Graphics graphics)
        {
            const int size = 8;
            int offset = size / 2;

            foreach (var point in _pointsToVisit)
            {
                graphics.FillEllipse(Brushes.DarkBlue, point.X - offset, point.Y - offset, size, size);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (GA != null)
            {
                GA.Stop();
                Thread.Sleep(2000);
            }
            SetupGA();
            Task.Run(() => GA.Run());
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (GA != null)
            {
                GA.Stop();
                Thread.Sleep(2000);
            }
            SetupPoints();
            panel1.Refresh();
        }
    }

    internal class Route
    {
        public Point[] Points { get; set; }

        public Route(params Point[] points)
        {
            Points = points;
        }
    }
}
