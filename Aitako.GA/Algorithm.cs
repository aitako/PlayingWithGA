﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Aitako.GA
{
    public class Algorithm<T>
    {
        internal List<T> Population
        {
            get { return _population ?? (_population = new List<T>()); }
            private set { _population = value; }
        }

        public delegate void IterationComplete(IterationResult<T> result);

        public event IterationComplete OnIterationCompleted;

        protected virtual void OnOnIterationCompleted(IterationResult<T> result)
        {
            IterationComplete handler = OnIterationCompleted;
            if (handler != null) handler(result);
        }

        private int _iteration;
        private double _fitness;
        private List<T> _population;
        internal int PopulationSize { get; private set; }
        private Func<T> _getRandomGenome;
        private Func<T, double> _evaluateFitness;
        private int _percentageOfFittestGenomesToBreed;
        private Func<T, T, T[]> _crossOverFunction;
        private bool _elitism;
        private BreedingStrategy _breedingStrategy;
        public Func<T, T, bool> _comparitor { get; set; }
        public int _mutationRate;
        public Func<int, T, T> _mutationFunction;
        private bool _running;
        protected Random RandomNumber { get; private set; }

        public int GetRandom(int low, int high)
        {
            return RandomNumber.Next(low, high);
        }

        public int GetGenerationNumber()
        {
            return _iteration;
        }

        public double GetFitness()
        {
            return _fitness;
        }

        public Algorithm()
        {
            _breedingStrategy = BreedingStrategy.Random;
            RandomNumber = new Random();
        }


        public Algorithm<T> UsingGenomeGenerationFunction(Func<T> getRandomGenomeFunction)
        {
            _getRandomGenome = getRandomGenomeFunction;
            return this;
        }

        public Algorithm<T> UsingMutationFunction(Func<int, T, T> mutation)
        {
            _mutationFunction = mutation;
            return this;
        }

        public Algorithm<T> UsingPercentageOfFittestGenomesToBreed(int percentage)
        {
            _percentageOfFittestGenomesToBreed = percentage;
            return this;
        }

        public Algorithm<T> WithFitnessFunction(Func<T, double> fitnessFunction)
        {
            _evaluateFitness = fitnessFunction;
            return this;
        }

        public Algorithm<T> WithComparitor(Func<T, T, bool> comparator)
        {
            _comparitor = comparator;
            return this;
        }

        public Algorithm<T> UsingBreedingStrategy(BreedingStrategy strategy)
        {
            _breedingStrategy = strategy;
            return this;
        }

        public Algorithm<T> WithElitism()
        {
            _elitism = true;
            return this;
        }

        public Algorithm<T> WithMutationRate(int rate)
        {
            _mutationRate = rate;
            return this;
        }

        public Algorithm<T> UsingCrossOverFunction(Func<T, T, T[]> crossOverFunction)
        {
            _crossOverFunction = crossOverFunction;
            return this;
        }

        public Algorithm<T> WithPopulationSize(int populationSize)
        {
            PopulationSize = populationSize;
            return this;
        }

        public Algorithm<T> Initialise()
        {
            if (Population.Count == 0)
            {
                return this.Populate(ReplenishPopulation());
            }

            if (_percentageOfFittestGenomesToBreed.Equals(0))
            {
                throw new Exception("Please set a value for the percentage of fittest genomes to breed.");
            }

            if (_crossOverFunction == null)
            {
                throw new Exception("Please provide a cross over function.");
            }

            return this;
        }

        public Algorithm<T> Populate(IEnumerable<T> initialPopulation)
        {
            Population.AddRange(initialPopulation);
            return Initialise();
        }

        private IEnumerable<T> ReplenishPopulation()
        {
            int i = 0;
            var generatedPopulation = new List<T>();
            while (i < PopulationSize)
            {
                generatedPopulation.Add(_getRandomGenome());
                i++;
            }
            return generatedPopulation;
        }

        internal IList<T> SelectMostFitGenomes()
        {
            int genomesToSelect = (int)((Population.Count / 100.0) * _percentageOfFittestGenomesToBreed);
            var selection = Population.OrderByDescending(i => _evaluateFitness(i)).Take(genomesToSelect);
            return selection.ToList();
        }

        public void Run()
        {
            _running = true;
            IterationResult<T> result;
            _iteration = 0;
            do
            {
                result = Iterate();
                OnOnIterationCompleted(result);
                _iteration++;
            } while (_running);
        }

        public void Stop()
        {
            _running = false;
        }

        public IterationResult<T> Iterate()
        {
            var fittestParentGenomes = SelectMostFitGenomes();

            Population.Clear();
           
            var offspring = new List<T>();
            //if (_breedingStrategy == BreedingStrategy.Random)
            //{
            //    ApplyPromisciousBreedingStrategy(fittestParentGenomes, offspring);
            //}
            //else if (_breedingStrategy == BreedingStrategy.Random)
            //{
            //    ApplyPairedBreedingStrategy(fittestParentGenomes, offspring);
            //}
            //else if (_breedingStrategy == BreedingStrategy.Random)
            //{
                ApplyRandomBreedingStrategy(fittestParentGenomes, offspring);
            //}

            if (_elitism)
            {
                Population.AddRange(fittestParentGenomes.Take(fittestParentGenomes.Count / 2));
            }

            Population.AddRange(offspring);

            // replenish population with random items
            if (Population.Count() < PopulationSize)
            {
                Population.AddRange(ReplenishPopulation());
            }

            var fittest = Population.Select(i => new IterationResult<T>(i, _evaluateFitness(i))).OrderByDescending(z => z.Fitness).FirstOrDefault();
            _fitness = fittest.Fitness;
            return fittest;
        }

        private void ApplyRandomBreedingStrategy(IList<T> fittestParentGenomes, List<T> offspring)
        {
            foreach (T parent in fittestParentGenomes)
            {
                var randomIndex = RandomNumber.Next(fittestParentGenomes.Count());
                var children = _crossOverFunction(parent, fittestParentGenomes[randomIndex]);

                for (var i = 0; i < children.Count(); i++)
                {
                    children[i] = _mutationFunction(_mutationRate, children[i]);
                }

                offspring.AddRange(children);
            }
        }

        private void ApplyPromisciousBreedingStrategy(IList<T> fittestParentGenomes, List<T> offspring)
        {
            bool stop = false;
            foreach (var genome in fittestParentGenomes)
            {
                T mate1 = genome;
                foreach (var mate2 in fittestParentGenomes.Where(i => !i.Equals(mate1)))
                {
                    var children = _crossOverFunction(mate1, mate2);

                    for (var i = 0; i < children.Count(); i++)
                    {
                        children[i] = _mutationFunction(_mutationRate, children[i]);
                    }

                    offspring.AddRange(children);

                    if (offspring.Count == PopulationSize)
                    {
                        stop = true;
                        break;
                    }
                }
                if (stop)
                {
                    break;
                }
            }
        }

        private void ApplyPairedBreedingStrategy(IList<T> fittestParentGenomes, List<T> offspring)
        {
            bool stop = false;
            for (var p1 = 0; p1 < fittestParentGenomes.Count; p1++)
            {
                for (var p2 = 2; p2 < fittestParentGenomes.Count - 2; p2++)
                {
                    if (_comparitor(fittestParentGenomes[p1], fittestParentGenomes[p2]))
                    {
                        continue;
                    }

                    var children = _crossOverFunction(fittestParentGenomes[p1], fittestParentGenomes[p2]);
                    for (var i = 0; i < children.Count(); i++)
                    {
                        children[i] = _mutationFunction(_mutationRate, children[i]);
                    }

                    offspring.AddRange(children);

                    if (offspring.Count == PopulationSize)
                    {
                        stop = true;
                        break;
                    }
                }
                if (stop)
                {
                    break;
                }
            }
        }
    }
}