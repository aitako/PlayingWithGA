﻿namespace Aitako.GA
{
    public class IterationResult<T>
    {
        public double Fitness { get; private set; }
        public T Genome { get; private set;}

        public IterationResult(T genome, double fitness)
        {
            Genome = genome;
            Fitness = fitness;
        }
    }
}