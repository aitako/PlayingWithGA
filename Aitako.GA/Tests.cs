﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Aitako.GA
{
    [TestFixture]
    public class BaseTests
    {
        const int PopulationSize = 10000;

        [Test]
        public void CanConstructWithRandomPopulation()
        {
            var ga = new Algorithm<Genome>()
                .UsingGenomeGenerationFunction(() => new Genome(1)).
                WithFitnessFunction(genome => 1).
                WithPopulationSize(PopulationSize)
                .UsingPercentageOfFittestGenomesToBreed(50)
                .UsingCrossOverFunction((genome, genome1) => new Genome[] { genome, genome1 })
                .Initialise();

            Assert.That(ga.Population, Is.Not.Null);
        }

        [Test]
        public void CanPopulateWithInitialPopulation()
        {
            var ga = new Algorithm<Genome>()
                .UsingGenomeGenerationFunction(() => new Genome(1)).
                WithFitnessFunction(genome => 1).
                WithPopulationSize(PopulationSize)
                .UsingCrossOverFunction((genome, genome1) => new Genome[] { genome, genome1 })
                .UsingPercentageOfFittestGenomesToBreed(50)
                 .Initialise();

            Assert.That(ga.Population.Count, Is.EqualTo(PopulationSize));
        }

        [Test]
        public void CanSelectFittestPercentageOfPopulation()
        {
            var random = new Random();
            var ga = new Algorithm<Genome>()
                .UsingGenomeGenerationFunction(() => new Genome(random.Next(1000)))
                .WithFitnessFunction(genome => 1000.0 - genome.Value)
                .WithPopulationSize(PopulationSize)
                .UsingCrossOverFunction((genome, genome1) => new Genome[] { genome, genome1 })
                .UsingPercentageOfFittestGenomesToBreed(50)
                .Initialise();

            const int percentage = 50;
            var selection = ga.SelectMostFitGenomes();

            Assert.That(selection.Count, Is.EqualTo(PopulationSize / 2));
        }

        [Test]
        public void FitnessFunctionIsRunCorrectly()
        {
            var random = new Random();
            var ga = new Algorithm<Genome>()
                        .UsingGenomeGenerationFunction(() => new Genome(random.Next(10)))
                        .WithFitnessFunction(genome => genome.Value % 2 == 0 ? 1 : 0)
                        .WithPopulationSize(PopulationSize)
                        .UsingCrossOverFunction((genome, genome1) => new Genome[] { genome, genome1 })
                        .UsingPercentageOfFittestGenomesToBreed(50)
                        .Initialise();

            const int percentage = 50;
            var selection = ga.SelectMostFitGenomes();
            foreach (var genome in selection)
            {
                Assert.That(genome.Value % 2 == 0);
            }
        }

        [Test]
        public void CanIteratePaired()
        {
            var random = new Random();
            var ga = new Algorithm<Genome>()
                            .UsingGenomeGenerationFunction(() => new Genome(random.Next(10)))
                            .WithFitnessFunction(genome => genome.Value % 2 == 0 ? 1 : 0)
                            .UsingCrossOverFunction((genome, genome1) => new Genome[] { new Genome((genome.Value * genome1.Value) / 2), new Genome((genome.Value * genome1.Value) / 2) })
                            .WithPopulationSize(PopulationSize)
                            .UsingPercentageOfFittestGenomesToBreed(50)
                            .UsingBreedingStrategy(BreedingStrategy.Random)
                            .Initialise();

            var result = ga.Iterate();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Genome, Is.Not.Null);
            Assert.That(result.Fitness, Is.Not.Null);
        }

        [Test]
        public void CanIterateRandom()
        {
            var random = new Random();
            var ga = new Algorithm<Genome>()
                            .UsingGenomeGenerationFunction(() => new Genome(random.Next(10)))
                            .WithFitnessFunction(genome => genome.Value % 2 == 0 ? 1 : 0)
                            .UsingCrossOverFunction((genome, genome1) => new Genome[] { new Genome((genome.Value * genome1.Value) / 2), new Genome((genome.Value * genome1.Value) / 2) })
                            .WithPopulationSize(PopulationSize)
                            .UsingPercentageOfFittestGenomesToBreed(50)
                            .UsingBreedingStrategy(BreedingStrategy.Random)
                            .Initialise();

            var result = ga.Iterate();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Genome, Is.Not.Null);
            Assert.That(result.Fitness, Is.Not.Null);
        }

        [Test]
        public void CanIteratePromiscious()
        {
            var random = new Random();
            var ga = new Algorithm<Genome>()
                            .UsingGenomeGenerationFunction(() => new Genome(random.Next(10)))
                            .WithFitnessFunction(genome => genome.Value % 2 == 0 ? 1 : 0)
                            .UsingCrossOverFunction((genome, genome1) => new Genome[] { new Genome((genome.Value * genome1.Value) / 2), new Genome((genome.Value * genome1.Value) / 2) })
                            .WithPopulationSize(PopulationSize)
                            .UsingPercentageOfFittestGenomesToBreed(50)
                            .UsingBreedingStrategy(BreedingStrategy.Random)
                            .Initialise();

            var result = ga.Iterate();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Genome, Is.Not.Null);
            Assert.That(result.Fitness, Is.Not.Null);
        }
    }

    internal class Genome
    {
        public int Value { get; set; }

        public Genome(int i)
        {
            Value = i;
        }
    }
}
